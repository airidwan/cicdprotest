using FluentAssertions;
using WinformGitlab;

namespace WinformGitlab_Test
{
    public class CalculatorTests
    {
        [Fact]
        public void Add_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var calculator = new Calculator();
            double num1 = 3;
            double num2 = 3;

            // Act
            var result = calculator.Add(
                num1,
                num2);

            // Assert
            result.Should().Be(6);
        }

        [Fact]
        public void Substract_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var calculator = new Calculator();
            double num1 = 12;
            double num2 = 6;

            // Act
            var result = calculator.Substract(
                num1,
                num2);

            // Assert
            result.Should().Be(6);
        }

        [Fact]
        public void divide_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var calculator = new Calculator();
            double num1 = 12;
            double num2 = 2;

            // Act
            var result = calculator.divide(
                num1,
                num2);

            // Assert
            result.Should().Be(6);
        }

        [Fact]
        public void Multiply_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var calculator = new Calculator();
            double num1 = 2;
            double num2 = 3;

            // Act
            var result = calculator.Multiply(
                num1,
                num2);

            // Assert
            result.Should().Be(6);
        }
    }
}